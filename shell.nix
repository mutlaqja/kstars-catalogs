{ pkgs ? import <nixos> {}
}:
let
  pydata-sphinx-theme = pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "pydata-sphinx-theme";
    version = "0.6.3";

    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "055bh3hyh72pafiylvgpsjlk18wm15gg4azc5rjlsww5z475iq1j";
    };

    patches = [
      ./pydata-patch.patch
    ];

    propagatedBuildInputs = with pkgs.python3Packages; [ docutils sphinx
                                                         pyyaml beautifulsoup4
                                                         click ];

    doCheck = false;
   };

  sphinx-book-theme = pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "sphinx-book-theme";
    version = "0.1.0";

    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "1gavcx31a3sknknisd1i5xvcvfdhqwm34q2n9a3gfhx8p578inxj";
    };

    patches= [
      ./sphinx-book.patch
    ];

    propagatedBuildInputs = with pkgs.python3Packages; [ docutils sphinx
                                                         pyyaml beautifulsoup4
                                                         click pydata-sphinx-theme ];
    doCheck = false;
  };

  sphinx-click = pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "sphinx-click";
    version = "3.0.0";

    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "17hgqwwjbd8xndfyfr03lf7hsb9zxf0h4wi2kf3amqgajali03az";
    };

    propagatedBuildInputs = with pkgs.python3Packages; [ docutils sphinx
                                                         click pbr ];
    doCheck = false;
  };

  sphinx-reload = pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "sphinx-reload";
    version = "0.2.0";

    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "01zs2bvwmfvqx4ld05m5qi1h67dscpslxhgrwm70xpm6zszhf3l0";
    };

    propagatedBuildInputs = with pkgs.python3Packages; [ livereload ];
    doCheck = false;
  };

  my-python-packages = python-packages: with python-packages; [
    pybind11
    flake8
    jedi
    virtualenv
    mypy
    click
    astropy
    pandas
    wget
    astroquery
    setuptools
    pydot
    sphinx
    sphinx-book-theme
    sphinx-click
    sphinx-reload
  ];

  python-with-my-packages = pkgs.python3.withPackages my-python-packages;
in pkgs.mkShell rec {
  name = "dev-shell";
  nativeBuildInputs = with pkgs; [ extra-cmake-modules ];
  inputsFrom = with pkgs; [ kstars ];
  buildInputs = with pkgs; [ ccache ninja qt5.full kstars python-with-my-packages fish sqlite sqlitebrowser black qtcreator qt5.wrapQtAppsHook breeze-qt5 qt5ct breeze-icons];
  QT_PLUGIN_PATH = with pkgs.qt514; "${qtbase}/${qtbase.qtPluginPrefix}/";
  hardeningDisable = [ "all" ];
  CPATH = pkgs.lib.makeSearchPathOutput "dev" "include" buildInputs;
  CXXPATH = pkgs.lib.makeSearchPathOutput "dev" "include" buildInputs;
  shellHook = ''
        export MYPYPATH="$PWD/stubs";
        export PYTHONPATH="$PYTHONPATH:/home/hiro/Documents/Projects/kstars/build/kstars/python/"
    '';
}
